This is a simple model editor to edit the .cub files from data1.db file.

Requires: LWJGL

To load the file you must specify it in the first argument when running the program.

Usage:

	P - Pick color

	S - Save file (will overwrite old file)

	Middle mouse to rotate around the model, and zoom it.

	Left mouse button creates new voxels.

	Right mouse button deletes voxels.

It can't go outside the original size, and there is no undo/redo options yet.

By MAT4DOR