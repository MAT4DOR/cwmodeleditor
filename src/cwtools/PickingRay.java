package cwtools;

public class PickingRay {
	private Vector3 origin;
	private Vector3 direction;
	public PickingRay(Vector3 origin, Vector3 dir) {
		this.origin = origin;
		this.direction = dir;
	}
	
	public Vector3 getOrigin() {
		return origin;
	}
	
	public Vector3 getDirection() {
		return direction;
	}
}
