package cwtools;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static org.lwjgl.opengl.GL11.*;

public class Model {
	protected int width, height, length;
	protected byte[][][][] colors;
	public Model(int width, int height, int length) {
		this.width = width;
		this.height = height;
		this.length = length;
		colors = new byte[width][height][length][3];
	}
	public static int littleInt(DataInputStream in) throws IOException {
		byte[] w = new byte[4];
		in.readFully(w, 0, 4);
		return w[3] << 24 | (w[2] & 0xff) << 16 | (w[1] & 0xff) << 8 | (w[0] & 0xff);
	}
	public void writeInt(DataOutputStream out, int i) throws IOException {
		out.write(i & 0xFF);
		out.write((i >>> 8) & 0xFF);
		out.write((i >>> 16) & 0xFF);
		out.write((i >>> 24) & 0xFF);
	}
	public static Model createFromStream(InputStream stream) throws IOException {
		DataInputStream in = new DataInputStream(stream);
		Model model = new Model(littleInt(in), littleInt(in), littleInt(in));
		for(int z = 0; z < model.length; z++) {
			for(int y = 0; y < model.height; y++) {
				for(int x = 0; x < model.width; x++) {
					model.colors[x][y][z][0] = in.readByte();// (byte) (-128+in.read());
					model.colors[x][y][z][1] = in.readByte();
					model.colors[x][y][z][2] = in.readByte();
				}
			}
		}
		in.close();
		return model;
	}
	public void save(OutputStream stream) throws IOException {
		DataOutputStream out = new DataOutputStream(stream);
		writeInt(out, width);
		writeInt(out, height);
		writeInt(out, length);
		for(int z = 0; z < length; z++) {
			for(int y = 0; y < height; y++) {
				for(int x = 0; x < width; x++) {
					out.writeByte(colors[x][y][z][0]);
					out.writeByte(colors[x][y][z][1]);
					out.writeByte(colors[x][y][z][2]);
				}
			}
		}
		out.close();
	}
	public void render() {
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				for(int z = 0; z < length; z++) {
					if(colors[x][y][z][0] == 0 && colors[x][y][z][1] == 0 && colors[x][y][z][2] == 0)
						continue;
					glColor3f(
							CWModelEditor.floatColor(colors[x][y][z][0]),
							CWModelEditor.floatColor(colors[x][y][z][1]),
							CWModelEditor.floatColor(colors[x][y][z][2]));
					glPushMatrix();
					glTranslatef(x, y, z);
					renderCube();
					glPopMatrix();
				}
			}
		}
	}

	public static void renderCube() {
		glBegin(GL_QUADS);
		glVertex3f(1.0f,1.0f,0.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(0.0f,1.0f,1.0f);
		glVertex3f(1.0f,1.0f,1.0f);
		glVertex3f(1.0f,0.0f,1.0f);
		glVertex3f(0.0f,0.0f,1.0f);
		glVertex3f(0.0f,0.0f,0.0f);
		glVertex3f(1.0f,0.0f,0.0f);
		glVertex3f(1.0f,1.0f,1.0f);
		glVertex3f(0.0f,1.0f,1.0f);
		glVertex3f(0.0f,0.0f,1.0f);
		glVertex3f(1.0f,0.0f,1.0f);
		glVertex3f(1.0f,0.0f,0.0f);
		glVertex3f(0.0f,0.0f,0.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(1.0f,1.0f,0.0f);
		glVertex3f(0.0f,1.0f,1.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(0.0f,0.0f,0.0f);
		glVertex3f(0.0f,0.0f,1.0f);
		glVertex3f(1.0f,1.0f,0.0f);
		glVertex3f(1.0f,1.0f,1.0f);
		glVertex3f(1.0f,0.0f,1.0f);
		glVertex3f(1.0f,0.0f,0.0f);
		glEnd();
	}

	public VoxelCollision getCollision(PickingRay ray, float maxDistance) {
		int x = (int)Math.floor(ray.getOrigin().x);
		int y = (int)Math.floor(ray.getOrigin().y);
		int z = (int)Math.floor(ray.getOrigin().z);


		float dx = ray.getDirection().x;
		float dy = ray.getDirection().y;
		float dz = ray.getDirection().z;

		int stepX = MathHelper.sign(dx);
		int stepY = MathHelper.sign(dy);
		int stepZ = MathHelper.sign(dz);

		float tMaxX = tmax(x, ray.getOrigin().x, dx);
		float tMaxY = tmax(y, ray.getOrigin().y, dy);
		float tMaxZ = tmax(z, ray.getOrigin().z, dz);

		float tDeltaX = (float)stepX/dx;
		float tDeltaY = (float)stepY/dy;
		float tDeltaZ = (float)stepZ/dz;


		maxDistance /= ray.getDirection().length();

		VoxelFace face = VoxelFace.SELF;
		while ((stepX > 0 ? x < width : x >= 0) &&
				(stepY > 0 ? y < height : y >= 0) &&
				(stepZ > 0 ? z < length : z >= 0)) {
			if (!(x < 0 || y < 0 || z < 0 || x >= width || y >= height || z >= length || (colors[x][y][z][0] == 0 && colors[x][y][z][1] == 0 && colors[x][y][z][2] == 0)))
				return new VoxelCollision(x, y, z, face);
			if (tMaxX < tMaxY) {
				if (tMaxX < tMaxZ) {
					if (tMaxX > maxDistance) break;
					x += stepX;
					tMaxX += tDeltaX;
					face = stepX < 0 ? VoxelFace.WEST : VoxelFace.EAST;
				} else {
					if (tMaxZ > maxDistance) break;
					z += stepZ;
					tMaxZ += tDeltaZ;
					face = stepZ > 0 ? VoxelFace.SOUTH : VoxelFace.NORTH;
				}
			} else {
				if (tMaxY < tMaxZ) {
					if (tMaxY > maxDistance) break;
					y += stepY;
					tMaxY += tDeltaY;
					face = stepY > 0 ? VoxelFace.BOTTOM : VoxelFace.TOP;
				} else {
					if (tMaxZ > maxDistance) break;
					z += stepZ;
					tMaxZ += tDeltaZ;
					face = stepZ > 0 ? VoxelFace.SOUTH : VoxelFace.NORTH;
				}
			}
		}
		return null;
	}
	private float tmax(int fs, float s, float ds) {
		return ds < 0 ? (((float)fs) - s)/ds : (((float)fs+1) - s)/ds;
	}
	public boolean isValid(int x, int y, int z) {
		return x >= 0 && x < width && y >= 0 && y < height && z >= 0 && z < length;
	}
}
