package cwtools;

public enum VoxelFace {
	WEST(1,0,0), EAST(-1,0,0),
	NORTH(0,0,1), SOUTH(0,0,-1),
	TOP(0,1,0), BOTTOM(0,-1,0),
	SELF(0,0,0);
	
	private Vector3 vec;
	private VoxelFace(int x, int y, int z){
		vec = new Vector3(x, y, z);
	}
	
	public Vector3 getVector() {
		return vec;
	}
}
