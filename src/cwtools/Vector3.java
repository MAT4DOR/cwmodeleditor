package cwtools;

public class Vector3 {
	public static final Vector3 EX = new Vector3(1f,0f,0f);
	public static final Vector3 EY = new Vector3(0f,1f,0f);
	public static final Vector3 EZ = new Vector3(0f,0f,1f);
	public static final Vector3 Zero = new Vector3(0f,0f,0f);
	
	public float x;
	public float y;
	public float z;
	
	public Vector3() {
	}
	
	public Vector3(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vector3 set(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		return this;
	}
	public Vector3 set(Vector3 vec) {
		this.x = vec.x;
		this.y = vec.y;
		this.z = vec.z;
		return this;
	}
	
	public Vector3 add(float x, float y, float z) {
		this.x += x;
		this.y += y;
		this.z += z;
		return this;
	}
	
	public Vector3 add(Vector3 vec) {
		x += vec.x;
		y += vec.y;
		z += vec.z;
		return this;
	}
	
	public Vector3 sub(float x, float y, float z) {
		this.x -= x;
		this.y -= y;
		this.z -= z;
		return this;
	}
	
	public Vector3 sub(Vector3 vec) {
		x -= vec.x;
		y -= vec.y;
		z -= vec.z;
		return this;
	}

	public Vector3 mul(float k) {
		x *= k;
		y *= k;
		z *= k;
		return this;
	}
	
	public Vector3 mul(Vector3 vec) {
		x *= vec.x;
		y *= vec.y;
		z *= vec.z;
		return this;
	}
	
	public Vector3 div(float k) {
		x /= k;
		y /= k;
		z /= k;
		return this;
	}
	
	public Vector3 div(Vector3 vec) {
		x /= vec.x;
		y /= vec.y;
		z /= vec.z;
		return this;
	}
	
	public Vector3 normalize() {
		float norm = length();
		x /= norm;
		y /= norm;
		z /= norm;
		return this;
	}
	
	public Vector3 negate() {
		x *= -1;
		y *= -1;
		z *= -1;
		return this;
	}
	
	public float dot(Vector3 vec) {
		return x*vec.x + y*vec.y + z*vec.z;
	}
	
	public Vector3 cross(Vector3 vec) {
		float oldX = x;
		float oldY = y;
		x = y*vec.z - z*vec.y;
		y = z*vec.x - oldX*vec.z;
		z = oldX*vec.y - oldY*vec.x;
		return this;
	}
	
	public Vector3 project(Vector3 vec) {
		Vector3 v = vec.normalize();
		return v.mul(this.dot(v));
	}
	
	public float length() {
		return (float) Math.sqrt(lengthSquared());
	}
	
	public float lengthSquared() {
		return x*x + y*y + z*z;
	}
	
	public Vector3 clone() {
		return new Vector3(x, y, z);
	}
	
	@Override
	public String toString() {
		return x + "," + y + "," + z;
	}

	public boolean isZero() {
		return x == 0f && y == 0f && z == 0f;
	}
}
