package cwtools;

public class MathHelper {
	public static final float HALF_PI = (float) (Math.PI/2.0f);
	public static final float PI = (float) (Math.PI);
	public static final float TWO_PI = (float) (Math.PI*2.0f);
	public static final float PI4 = (float) (Math.PI/4.0f);
	public static final float PI8 = (float) (Math.PI/8.0f);
	public static final float PI16 = (float) (Math.PI/16.0f);
	public static float toRadians(float angle) {
		return (float) (angle * Math.PI / 180.0);
	}
	public static float toDegrees(float angle) {
		return (float) (angle * 180.0 / Math.PI);
	}
	public static double clamp(double val, double min, double max) {
		return (val < min ? min : (val > max ? max : val));
	}
	public static float clamp(float val, float min, float max) {
		return (val < min ? min : (val > max ? max : val));
	}
	public static float sin(double a) {
		return (float) Math.sin(a);
	}
	public static float cos(double a) {
		return (float) Math.cos(a);
	}
	public static float tan(float theta) {
		return (float) Math.tan(theta);
	}
	public static float distance(float x1, float y1, float x2, float y2) {
		return (float) Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
	}
	public static int sign(float f) {
		return f > 0 ? 1 : (f < 0 ? -1 : 0);
	}
}
