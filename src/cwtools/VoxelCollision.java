package cwtools;


public class VoxelCollision {
	private int x, y, z;
	private VoxelFace face;
	public VoxelCollision(int x, int y, int z, VoxelFace face) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.face = face;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public int getZ() {
		return z;
	}
	public Vector3 getPosition() {
		return new Vector3(x, y, z);
	}
	public VoxelFace getFace() {
		return face;
	}
	@Override
	public String toString() {
		return "(" + x + "," + y + "," + z + "," + face + ")";
	}
}
