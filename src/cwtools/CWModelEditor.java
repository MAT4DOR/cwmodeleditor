package cwtools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import static org.lwjgl.util.glu.GLU.*;
import static org.lwjgl.opengl.GL11.*;

public class CWModelEditor {
	
	private boolean running;
	
	private Model model;

	float width = 800f;
	float height = 600f;
	float fov = 60f;
	float znear = 0.01f;
	float zfar = 2000f;
	

	Vector3 lookAt;
	Vector3 eye = new Vector3();
	float phi = (float) (Math.PI/4f);
	float theta = (float) (Math.PI/2f);
	float distance = 40f;
	PickingRay ray;
	
	byte editorR = 0, editorG = 0, editorB = 0;
	Vector3 newBlock;
	
	File toModify;
	
	public void init() throws LWJGLException {
		Display.setTitle("Cube World Model Editor by MAT4DOR");
		Display.setDisplayMode(new DisplayMode(800, 600));
		Display.create();
		
		Keyboard.create();
		Mouse.create();
		glEnable(GL_DEPTH_TEST);
		glClearColor(0.3f, 0.7f, 0.9f, 1.0f);
		
		try {
			model = Model.createFromStream(new FileInputStream(toModify));
			lookAt = new Vector3((float)model.width/2f, (float)model.height/2f, (float)model.length/2f);
			distance = (float) Math.sqrt(model.width*model.width + model.height*model.height + model.length*model.length)*1.5f;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void loop() {
		running = true;
		while(running) {
			if(Display.isCloseRequested()) {
				running = false;
				break;
			}
			update();
			render();
			Display.update();
		}
	}
	public void update() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(fov, width/height, znear, zfar);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		if(Mouse.isButtonDown(2)) {
			theta -= Math.toRadians(Mouse.getDX()/180.0f) * 20f;
			phi += Math.toRadians(Mouse.getDY()/180.0f) * 20f;

			phi = MathHelper.clamp(phi, 0.02f, MathHelper.HALF_PI-0.02f);
			theta = (float) (theta % MathHelper.TWO_PI);
			if(theta < 0)
				theta += MathHelper.TWO_PI;
		}
		distance += Mouse.getDWheel()/60;
		if(distance < 2)
			distance = 2f;
		
		eye.set(lookAt);
		eye.add((float) (distance * Math.sin(phi) * Math.cos(theta)),(float) (distance * Math.sin(phi) * Math.sin(theta)),(float) (distance * Math.cos(phi)));
		
		gluLookAt(eye.x, eye.y, eye.z, lookAt.x, lookAt.y, lookAt.z, 0.0f, 0.0f, 1.0f);

		ray = getPickingRay();
		VoxelCollision coll = model.getCollision(ray, 100f);
		
		if(coll != null)
			newBlock = coll.getPosition().clone().add(coll.getFace().getVector());
		else
			newBlock = null;
		
		while(Mouse.next()) {
			if(Mouse.isButtonDown(0))
				checkColorGrabbers();
			if(Mouse.getEventButtonState() && (Mouse.isButtonDown(0) || Mouse.isButtonDown(1))) {
				if(coll != null) {
					if(Mouse.isButtonDown(1)) {
						int x = coll.getX();
						int y = coll.getY();
						int z = coll.getZ();
						if(model.isValid(x, y, z)) {
							model.colors[x][y][z][0] = 0;
							model.colors[x][y][z][1] = 0;
							model.colors[x][y][z][2] = 0;
						}
					} else {
						int x = (int) newBlock.x;
						int y = (int) newBlock.y;
						int z = (int) newBlock.z;
						if(model.isValid(x, y, z)) {
							model.colors[x][y][z][0] = editorR;
							model.colors[x][y][z][1] = editorG;
							model.colors[x][y][z][2] = editorB;
						}
					}
				}
			}
			while(Keyboard.next()) {
				if(Keyboard.getEventKeyState() && Keyboard.getEventKey() == Keyboard.KEY_P) {
					if(coll != null) {
						int x = coll.getX();
						int y = coll.getY();
						int z = coll.getZ();
						if(model.isValid(x, y, z)) {
							editorR = model.colors[x][y][z][0];
							editorG = model.colors[x][y][z][1];
							editorB = model.colors[x][y][z][2];
						}
					}
				}
				if(Keyboard.getEventKeyState() && Keyboard.getEventKey() == Keyboard.KEY_S) {
					try {
						if(!toModify.exists())
							toModify.createNewFile();
						model.save(new FileOutputStream(toModify));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public static float floatColor(byte x) {
	    return (x & 0xFF)/255f;
	}
	public void render() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		model.render();
		
		if(newBlock != null && !(editorR == 0 && editorG == 0 && editorB == 0)) {
			int x = (int) newBlock.x;
			int y = (int) newBlock.y;
			int z = (int) newBlock.z;
			glColor3f(floatColor(editorR),floatColor(editorG),floatColor(editorB));
			glPushMatrix();
			glTranslatef(x, y, z);
			Model.renderCube();
			glPopMatrix();
		}

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, 800, 600, 0, 0, 1);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glColor3f(floatColor(editorR),floatColor(editorG),floatColor(editorB));
		glBegin(GL_QUADS);
			glVertex2f(32f, 32f);
			glVertex2f( 0f, 32f);
			glVertex2f( 0f,  0f);
			glVertex2f(32f,  0f);
		glEnd();
		
		drawSliderFor(editorR, 64, 6+3);
		drawSliderFor(editorG, 64, 6+3+7);
		drawSliderFor(editorB, 64, 6+3+14);
	}

	private void checkColorGrabbers() {
		int x = Mouse.getX();
		int y = (int) (height-Mouse.getY());
		if(x >= 64 && x < 64+256 && y > 6 && y <= 6+21) {
			int i = x-64;
			switch((y-6-1) / 7) {
				case 0:
					editorR = (byte)(i & 0xFF);
					break;
				case 1:
					editorG = (byte)(i & 0xFF);
					break;
				case 2:
					editorB = (byte)(i & 0xFF);
					break;
				default:
					throw new Error("Should never be here!!");
			}
			
		}
	}
	
	private void drawSliderFor(byte color, int x, int y) {
		glColor3f(0f, 0f, 0f);
		glPushMatrix();
		glTranslatef(x, y, 0);
		glBegin(GL_QUADS);
			glVertex2f(256f, 1f);
			glVertex2f( 0f, 1f);
			glVertex2f( 0f,  0f);
			glVertex2f(256f,  0f);
		glEnd();
		glTranslatef(color & 0xFF, -3f, 0);
		glBegin(GL_QUADS);
			glVertex2f(1f, 7f);
			glVertex2f( 0f, 7f);
			glVertex2f( 0f,  0f);
			glVertex2f(1f,  0f);
		glEnd();
		glPopMatrix();
	}
	public PickingRay getPickingRay() {
		float x = Mouse.getX();
		float y = height-Mouse.getY();

		Vector3 view = lookAt.clone().sub(eye).normalize();
		Vector3 h = view.clone().cross(Vector3.EZ).normalize();
		Vector3 v = view.clone().cross(h).normalize();
		
		float vLength = (float) (Math.tan( MathHelper.toRadians(fov) / 2f ) * znear);
		float hLength = vLength * (width / height);

		v.mul(vLength);
		h.mul(hLength);
		
		x -= width / 2f;
		y -= height / 2f;

		x /= (width / 2f);
		y /= (height / 2f);
		
		Vector3 pos = eye.clone().add(view.mul(znear)).add(h.mul(x)).add(v.mul(y));
		Vector3 dir = pos.clone().sub(eye).normalize();
		return new PickingRay(pos, dir);
	}
	
	public static void main(String[] args) throws LWJGLException {
		if(args.length == 1) {
			CWModelEditor editor = new CWModelEditor();
			editor.toModify = new File(args[0]);
			if(editor.toModify.exists() && !editor.toModify.isDirectory()) {
				editor.init();
				editor.loop();
			}
		} else {
			System.out.println("Cube World Model Editor by MAT4DOR - alpha01");
			System.out.println("Specify the file to open in the first argument.");
		}
	}
}
